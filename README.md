# ppDLI configuration

## Configuring the server
The file [server/test.yaml](server/test.yaml) contains the server configuration.
The file [server/example_fixtures.yaml](server/example_fixtures.yaml) contains the users/organizations/collaborations which can be imported.

Both files can be edited to reflect your own configuration/implementation.

## Running a ppDLI server locally
Execute the install_ubuntu.sh or install_macos.sh file, this will install the needed dependencies and start the server.

## Setting up a ppDLI server remotely
The ansible playbook included can be executed using the command `ansible-playbook install_generic.yml`.