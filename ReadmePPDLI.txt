// PPDLI Architecture 

Reference: https://github.com/IKNL/ppDLI 
           https://distributedlearning.readme.io/          (### !!!) The website instructions are outdated 
           
           
           
The following will guide you through the way from scratch to set up a node and server and perform a task. 


Step 1: Create a azure VNET, Gateway - VPN                                                //details to be added soon

Step 2: Create a azure VM - Ubuntu Server 18.04                                           //details to be added soon
            - accessible through a public ip (example http://13.94.188.17)
            - accessible through VPN (example http://12.0.0.4)
            - admin access needs a SSH2 private key
            - current running server needs a sudo user privilege (protected by password)
                
                
Step 3: Set up and run a server
             
    Step 3.1    - checkout the repository:  https://gitlab.com/PersonalHealthTrain/implementations/dutchmaastricht/ppdli-configuration 
    Step 3.2    - chmod +x /path/to/install_ubuntu.sh
                - ./install_ubuntu.sh
             
                   # installs git python3-venv python3-pip python3-dev postgresql gcc libpq-dev
                   # creates a virtual environment - venv and activates it
                   # installs psycopg                        // The original website instructions will give you an error with psycopg
                   # installs ppdli package
                   
                   # runs a server with the preset config files : server/test.yaml and server/example_fixtures.yaml
                   
                   You can also create a server instance by typing with user_defined config file:  ppserver new 
              
      ~~~ The server should be up and running at this instance at port number 5000 (default config, can be changed in config file)~~~
              
Step 4:  From the browser type : http://13.94.188.17:5000/apidocs or for VPN http://12.0.0.4:5000/apidocs

Step 5:  Setting up a node: 
                ppnode new : Follow the instructions , provide server ip for the server you want to set up your node with
                ppnode start : Will start the node tagged to the server ip
                
                        ====== Node-side & configuration given by J van Soest =====
                        *(unless already created a configuration file for the node) ppnode new*
                        *Give your node configuration a label.*
                        *Enter the api-key given to you by the server operator.*
                        *Enter the base-URL of the server eg : http://13.94.188.17*
                        *Enter port to which the server listens: 5000*
                        *Path of the api: /api*
                        *Database path:?*
                        *Defaults for the rest are fine.*
                        *(to start broadcasting as the node) ppnode start*
                        *(select which node configuration you want to broadcast)*

Step 6:  Check if node is up and running:  

Step 7:  User Operations
            
            1. Get List of Collaborations
            2. Get List of Organizations
            3. Get list of Nodes
            4. Add a task
            5. Get task list
            6. Retreive Result - by polling . Can also be done by a websocket
            7. Get result list
            8. Display result
            
            
            
            
GOAL AHEAD :
 
 1. Fix to add nodes without having to restart the server
 2. Clean up the server and start afresh
 3. Provide each researcher to set a node + user at his/her own pc
 4. Set up a central Maastro node with a FHIR and SPARQL endpoint (possibly more endpoints)
 5. Do More hands-on workshops and test runs with more research questions and data in coming weeks. 

